FROM gitlab-registry.cern.ch/lhcb-docker/python-deployment:python-3.5

# http://neuro.debian.net/install_pkg.html?p=singularity-container
# Try downloading the GPG keys multiple times to avoid unreliabe severs
RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get --yes install wget gnupg && \
    wget -O- http://neuro.debian.net/lists/stretch.de-md.libre > /etc/apt/sources.list.d/neurodebian.sources.list && \
    (apt-key adv --recv-keys --keyserver hkp://p80.pool.sks-keyservers.net:80 0xA5D32F012649A5A9 || \
     apt-key adv --recv-keys --keyserver hkp://ipv4.pool.sks-keyservers.net 0xA5D32F012649A5A9 || \
     apt-key adv --recv-keys --keyserver hkp://pgp.mit.edu:80 0xA5D32F012649A5A9 || \
     apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xA5D32F012649A5A9 || \
     apt-key adv --recv-keys --keyserver hkp://pool.sks-keyservers.net:80 0xA5D32F012649A5A9) && \
    apt-get update && \
    apt-get --yes install singularity-container && \
    rm -rf /var/lib/apt/lists/* /usr/share/man/man1
