FROM gitlab-registry.cern.ch/lhcb-docker/python-deployment:centos7
ARG HEP_OSLIBS_NAME=HEP_OSlibs

# Based on: https://www.sylabs.io/guides/3.2/user-guide/installation.html#install-rpm
# Install prerequisites, go, build+install singularity then cleanup
RUN yum update -y && \
    yum install -y ${HEP_OSLIBS_NAME} && \
    yum groupinstall -y 'Development Tools' && \
    yum install -y openssl-devel libuuid-devel libseccomp-devel wget squashfs-tools gzip xz tar && \
    export VERSION=1.12 OS=linux ARCH=amd64 && \
    wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && \
    tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz && \
    export GOPATH=${HOME}/go && \
    export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin && \
    export VERSION=3.2.0 && \
    wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-${VERSION}.tar.gz && \
    rpmbuild -tb --nodeps singularity-${VERSION}.tar.gz && \
    rpm -ivh ~/rpmbuild/RPMS/x86_64/singularity-$VERSION-1.*.x86_64.rpm && \
    rm -rf go$VERSION.$OS-$ARCH.tar.gz singularity-${VERSION}.tar.gz /usr/local/go ~/rpmbuild && \
    yum clean all && \
    rm -rf /var/lib/apt/lists/* /lib/modules/* /lib/firmware/* /lib/kbd /var/cache/yum
